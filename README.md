# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://poptomovd@bitbucket.org/poptomovd/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/poptomovd/stroboskop/commits/73f72d81f4b90950bf2f22fac342add1cf785b50

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/poptomovd/stroboskop/commits/23bc923333a2c78d6d9cbb33c8feb8c1f84c8620

Naloga 6.3.2:
https://bitbucket.org/poptomovd/stroboskop/commits/c4ed5c97ff720b973d26b90b50f7b9da604b547b

Naloga 6.3.3:
https://bitbucket.org/poptomovd/stroboskop/commits/e92a0b3f4976f04e32ef65fa37ab2c03923ee19b

Naloga 6.3.4:
https://bitbucket.org/poptomovd/stroboskop/commits/c7a856abc5935d0812c0a4bf37af9c72040fa4b7

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/poptomovd/stroboskop/commits/7d1d713ebb4e2c35d0f3027240f1d72e03b4436f

Naloga 6.4.2:
https://bitbucket.org/poptomovd/stroboskop/commits/c73cd6b07c2efdeab70b28d8c336c3e296c81b02

Naloga 6.4.3:
https://bitbucket.org/poptomovd/stroboskop/commits/b65ee69000954d1dbe8ba266ed0a49883086f791

Naloga 6.4.4:
https://bitbucket.org/poptomovd/stroboskop/commits/8607c7aac2bd092e4b0edd78e96fd3fe8f5f46d2